﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DomainModel;
using Services;

namespace BallisticCalculator
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            resultsGrid.Columns.Add("distance", "Distance M");
            resultsGrid.Columns.Add("velocity", "Velocity M/s");
            resultsGrid.Columns.Add("penetration", "Penetration cm");
            resultsGrid.Columns.Add("time", "Time s");
            resultsGrid.Columns.Add("drop", "Drop cm");
            resultsGrid.Columns.Add("drift", "Drift cm");
            resultsGrid.Columns.Add("momentum", "Momentum Kg*m/s");
            resultsGrid.Columns.Add("energy", "Energy J");
            resultsGrid.Columns[0].AutoSizeMode = DataGridViewAutoSizeColumnMode.ColumnHeader;
            resultsGrid.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.ColumnHeader;
            resultsGrid.Columns[2].AutoSizeMode = DataGridViewAutoSizeColumnMode.ColumnHeader;
            resultsGrid.Columns[3].AutoSizeMode = DataGridViewAutoSizeColumnMode.ColumnHeader; 
            resultsGrid.Columns[4].AutoSizeMode = DataGridViewAutoSizeColumnMode.ColumnHeader;
            resultsGrid.Columns[5].AutoSizeMode = DataGridViewAutoSizeColumnMode.ColumnHeader;
            resultsGrid.Columns[6].AutoSizeMode = DataGridViewAutoSizeColumnMode.ColumnHeader;
            resultsGrid.Columns[7].AutoSizeMode = DataGridViewAutoSizeColumnMode.ColumnHeader;

            resultsGrid.AllowUserToAddRows = false;
            resultsGrid.AllowUserToDeleteRows = false;
            resultsGrid.AllowUserToOrderColumns = true;
            resultsGrid.ReadOnly = true;
            resultsGrid.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            resultsGrid.MultiSelect = false;
            resultsGrid.AllowUserToResizeColumns = false;

            SphereDiameter_ValueChanged(null, null);
        }

        private void FireButton_Click(object sender, EventArgs e)
        {
            OldCalculator();
        }

        private void OldCalculator()
        {
            resultsGrid.Rows.Clear();

            int materialDensity = 11340;
            double caliberInMeters = (double)SphereDiameter.Value / 1000;
            double sphereVolume = 4 * Math.Pow(caliberInMeters / 2, 3) * 3.1415926 / 3;
            double sphereMass = materialDensity * sphereVolume;
            double sphereMassInGrains = (sphereMass * 1000) / 0.0648;
            double sphereArea = ((caliberInMeters / 2) * (caliberInMeters / 2)) * 3.1415926;

            OldCalculator oldCalculator = new OldCalculator();
            //IList<double> result = oldCalculator.Calculate((double)MuzzleVelocity.Value, (double)SphereDiameter.Value / 1000, materialDensity, 0, (double)AirTemperature.Value, 0);
            IList<FlightPosition> result = oldCalculator.CalculateFromFPS(((double)MuzzleVelocity.Value) / 0.3048, (double)SphereDiameter.Value / 25.4, sphereMassInGrains, (double)Crosswind.Value * 2.237, (double)AirTemperature.Value * 1.8 + 32, (double)Altitude.Value / 0.3048);

            int i = 0;
            foreach(FlightPosition flightPosition in result)
            {
                resultsGrid.Rows.Add(
                    flightPosition.Distance,
                    flightPosition.Velocity.ToString("0.00"),
                    ((sphereMass * flightPosition.Velocity / sphereArea) / 910).ToString("0.00"),
                    flightPosition.Time.ToString("0.000"),
                    (flightPosition.Drop * 100).ToString("0.00"),
                    (flightPosition.Drift * 100).ToString("0.00"),
                    (sphereMass * flightPosition.Velocity).ToString("0.00"), 
                    ((sphereMass * flightPosition.Velocity * flightPosition.Velocity) / 2).ToString("0.00"));
                i++;
            }
        }

        private void ShotCalculator()
        {
            ShotCalculatorService service = new ShotCalculatorService();
            decimal vaporPressure = service.CalculateVaporPressure(0, AirTemperature.Value);//RelativeAirHumidity.Value
            decimal airDensity = service.CalculateAirDensity(AirTemperature.Value, Altitude.Value, vaporPressure);
            decimal speedOfSound = service.CalculateSpeedOfSound(AirTemperature.Value, Altitude.Value, vaporPressure);
            decimal scaleFactorK = service.CalculateScaleFactorK(SphereDiameter.Value / 1000, 11340M, airDensity);
            decimal machNumber = service.CalculateMachNumber(MuzzleVelocity.Value, speedOfSound);

            resultsGrid.Rows.Clear();

            if (machNumber > 1.2M)
            {
                //testBox.Text = machNumber.ToString();
            }
            else if (machNumber > 0.7M)
            {
                //testBox.Text = machNumber.ToString();
            }
            else
            {

                for (int i = 0; i < 101; i += 5)
                {
                    //decimal velocity = (0.2926M * speedOfSound) / ((decimal)(Math.Pow(0.495, (double)(0.3135M * (i / scaleFactorK)))) - 0.077M);
                    decimal velocity = (0.418M * MuzzleVelocity.Value) / (((0.418M + 0.11M * machNumber) * (decimal)Math.Exp((double)(0.3135M * (i / scaleFactorK)))) - 0.11M * machNumber);
                    resultsGrid.Rows.Add(i.ToString(), velocity.ToString());
                }

            }
        }

        private void SphereDiameter_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar.Equals('.') || e.KeyChar.Equals(','))
            {
                e.KeyChar = ((System.Globalization.CultureInfo)System.Globalization.CultureInfo.CurrentCulture).NumberFormat.NumberDecimalSeparator.ToCharArray()[0];
            }
        }

        private void SphereDiameter_ValueChanged(object sender, EventArgs e)
        {
            double caliberInMeters = (double)SphereDiameter.Value / 1000;
            double sphereVolume = 4 * Math.Pow(caliberInMeters / 2, 3) * 3.1415926 / 3;
            double sphereMass = (double)materialDensity.Value * sphereVolume;
            sphereMassLbl.Text = (sphereMass * 1000).ToString("0.000");
        }
    }
}
