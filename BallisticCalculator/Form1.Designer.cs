﻿namespace BallisticCalculator
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.SphereDiameter = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.MuzzleVelocity = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.AirTemperature = new System.Windows.Forms.NumericUpDown();
            this.Altitude = new System.Windows.Forms.NumericUpDown();
            this.Crosswind = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.FireButton = new System.Windows.Forms.Button();
            this.resultsGrid = new System.Windows.Forms.DataGridView();
            this.label13 = new System.Windows.Forms.Label();
            this.sphereMassLbl = new System.Windows.Forms.Label();
            this.materialDensity = new System.Windows.Forms.NumericUpDown();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.SphereDiameter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MuzzleVelocity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AirTemperature)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Altitude)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Crosswind)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.resultsGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.materialDensity)).BeginInit();
            this.SuspendLayout();
            // 
            // SphereDiameter
            // 
            this.SphereDiameter.DecimalPlaces = 2;
            this.SphereDiameter.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.SphereDiameter.Location = new System.Drawing.Point(153, 24);
            this.SphereDiameter.Maximum = new decimal(new int[] {
            254,
            0,
            0,
            65536});
            this.SphereDiameter.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.SphereDiameter.Name = "SphereDiameter";
            this.SphereDiameter.Size = new System.Drawing.Size(87, 20);
            this.SphereDiameter.TabIndex = 3;
            this.SphereDiameter.Value = new decimal(new int[] {
            45,
            0,
            0,
            65536});
            this.SphereDiameter.ValueChanged += new System.EventHandler(this.SphereDiameter_ValueChanged);
            this.SphereDiameter.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.SphereDiameter_KeyPress);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(60, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(87, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Sphere diameter:";
            // 
            // MuzzleVelocity
            // 
            this.MuzzleVelocity.Location = new System.Drawing.Point(153, 81);
            this.MuzzleVelocity.Maximum = new decimal(new int[] {
            650,
            0,
            0,
            0});
            this.MuzzleVelocity.Minimum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.MuzzleVelocity.Name = "MuzzleVelocity";
            this.MuzzleVelocity.Size = new System.Drawing.Size(87, 20);
            this.MuzzleVelocity.TabIndex = 4;
            this.MuzzleVelocity.Value = new decimal(new int[] {
            400,
            0,
            0,
            0});
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(64, 83);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(83, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Muzzle Velocity:";
            // 
            // AirTemperature
            // 
            this.AirTemperature.DecimalPlaces = 1;
            this.AirTemperature.Location = new System.Drawing.Point(153, 107);
            this.AirTemperature.Maximum = new decimal(new int[] {
            60,
            0,
            0,
            0});
            this.AirTemperature.Minimum = new decimal(new int[] {
            40,
            0,
            0,
            -2147483648});
            this.AirTemperature.Name = "AirTemperature";
            this.AirTemperature.Size = new System.Drawing.Size(87, 20);
            this.AirTemperature.TabIndex = 5;
            this.AirTemperature.Value = new decimal(new int[] {
            20,
            0,
            0,
            0});
            // 
            // Altitude
            // 
            this.Altitude.Location = new System.Drawing.Point(153, 133);
            this.Altitude.Maximum = new decimal(new int[] {
            3000,
            0,
            0,
            0});
            this.Altitude.Name = "Altitude";
            this.Altitude.Size = new System.Drawing.Size(87, 20);
            this.Altitude.TabIndex = 6;
            // 
            // Crosswind
            // 
            this.Crosswind.Location = new System.Drawing.Point(153, 160);
            this.Crosswind.Maximum = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.Crosswind.Name = "Crosswind";
            this.Crosswind.Size = new System.Drawing.Size(87, 20);
            this.Crosswind.TabIndex = 8;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(66, 109);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(81, 13);
            this.label3.TabIndex = 9;
            this.label3.Text = "Air temperature:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(102, 135);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(45, 13);
            this.label4.TabIndex = 10;
            this.label4.Text = "Altitude:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(89, 162);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(58, 13);
            this.label6.TabIndex = 12;
            this.label6.Text = "Crosswind:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(246, 26);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(23, 13);
            this.label7.TabIndex = 13;
            this.label7.Text = "mm";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(246, 83);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(25, 13);
            this.label8.TabIndex = 14;
            this.label8.Text = "m/s";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(246, 109);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(14, 13);
            this.label9.TabIndex = 15;
            this.label9.Text = "C";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(246, 135);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(15, 13);
            this.label10.TabIndex = 16;
            this.label10.Text = "m";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(246, 162);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(25, 13);
            this.label12.TabIndex = 18;
            this.label12.Text = "m/s";
            // 
            // FireButton
            // 
            this.FireButton.Location = new System.Drawing.Point(153, 229);
            this.FireButton.Name = "FireButton";
            this.FireButton.Size = new System.Drawing.Size(75, 23);
            this.FireButton.TabIndex = 19;
            this.FireButton.Text = "Fire!";
            this.FireButton.UseVisualStyleBackColor = true;
            this.FireButton.Click += new System.EventHandler(this.FireButton_Click);
            // 
            // resultsGrid
            // 
            this.resultsGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.resultsGrid.Location = new System.Drawing.Point(309, 24);
            this.resultsGrid.Name = "resultsGrid";
            this.resultsGrid.Size = new System.Drawing.Size(678, 634);
            this.resultsGrid.TabIndex = 21;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(76, 51);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(71, 13);
            this.label13.TabIndex = 22;
            this.label13.Text = "Sphere mass:";
            // 
            // sphereMassLbl
            // 
            this.sphereMassLbl.AutoSize = true;
            this.sphereMassLbl.Location = new System.Drawing.Point(157, 51);
            this.sphereMassLbl.Name = "sphereMassLbl";
            this.sphereMassLbl.Size = new System.Drawing.Size(25, 13);
            this.sphereMassLbl.TabIndex = 23;
            this.sphereMassLbl.Text = "000";
            // 
            // materialDensity
            // 
            this.materialDensity.Location = new System.Drawing.Point(153, 186);
            this.materialDensity.Maximum = new decimal(new int[] {
            25000,
            0,
            0,
            0});
            this.materialDensity.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.materialDensity.Name = "materialDensity";
            this.materialDensity.Size = new System.Drawing.Size(87, 20);
            this.materialDensity.TabIndex = 24;
            this.materialDensity.Value = new decimal(new int[] {
            11340,
            0,
            0,
            0});
            this.materialDensity.ValueChanged += new System.EventHandler(this.SphereDiameter_ValueChanged);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(64, 188);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(83, 13);
            this.label14.TabIndex = 25;
            this.label14.Text = "Material density:";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(188, 51);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(13, 13);
            this.label15.TabIndex = 26;
            this.label15.Text = "g";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(246, 188);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(39, 13);
            this.label16.TabIndex = 27;
            this.label16.Text = "Kg/m3";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(999, 670);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.materialDensity);
            this.Controls.Add(this.sphereMassLbl);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.resultsGrid);
            this.Controls.Add(this.FireButton);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.Crosswind);
            this.Controls.Add(this.Altitude);
            this.Controls.Add(this.AirTemperature);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.MuzzleVelocity);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.SphereDiameter);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "Form1";
            this.Text = "Shotgun Ballistic Calculator";
            ((System.ComponentModel.ISupportInitialize)(this.SphereDiameter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MuzzleVelocity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AirTemperature)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Altitude)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Crosswind)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.resultsGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.materialDensity)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.NumericUpDown SphereDiameter;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown MuzzleVelocity;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown AirTemperature;
        private System.Windows.Forms.NumericUpDown Altitude;
        private System.Windows.Forms.NumericUpDown Crosswind;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Button FireButton;
        private System.Windows.Forms.DataGridView resultsGrid;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label sphereMassLbl;
        private System.Windows.Forms.NumericUpDown materialDensity;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
    }
}

