﻿using System;

namespace Services
{
    public class ShotCalculatorService
    {
        //public decimal CalculateVelocityForSubSonic(decimal distanceFromMuzzle)
        //{

        //}

        public decimal CalculateVaporPressure(decimal relativeHumidity, decimal temperature)
        {
            return 133.322M * (relativeHumidity / 100) * (decimal)Math.Pow(10, (double)(8.10765M - (1750.286M / (235 + temperature))));
        }

        public decimal CalculateAirDensity(decimal airTemperature, decimal airPressure, decimal vaporPressure)
        {
            decimal airPressureInHg = ConvertPressureToInHg(airPressure);
            decimal vaporPressureInHg = ConvertPressureToInHg(vaporPressure);
            return (airPressureInHg / (84.763M * (airTemperature + 273.15M))) * (1 - (0.378M * vaporPressureInHg) / airPressureInHg) * 1000;
        }

        public decimal CalculateSpeedOfSound(decimal airTemperature, decimal airPressure, decimal vaporPressure)
        {
            return 20.05M * (decimal)Math.Sqrt((double)(airTemperature + 273.15M)) * (1M + (0.14M * vaporPressure) / airPressure);
        }

        public decimal CalculateScaleFactorK(decimal sphereDiameter, decimal materialDensity, decimal airDensity)
        {
            return (sphereDiameter * materialDensity) / airDensity;
        }

        public decimal CalculateMachNumber(decimal muzzleVelocity, decimal speedOfSound)
        {
            return muzzleVelocity / speedOfSound;
        }

        public decimal ConvertPressureToInHg(decimal airPressure)
        {
            return airPressure * 0.00029530M;
        }

        public decimal ConvertPressureToPa(decimal airPressureInHg)
        {
            return airPressureInHg / 0.00029530M;
        }
    }
}
