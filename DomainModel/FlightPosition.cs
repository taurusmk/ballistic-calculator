﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DomainModel
{
    public class FlightPosition
    {
        public double Distance { get; set; }
        public double Velocity { get; set; }
        public double Time { get; set; }
        public double Drift { get; set; }
        public double Drop { get; set; }
    }
}
