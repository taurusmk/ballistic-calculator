﻿using DomainModel;
using System;
using System.Collections.Generic;
using System.Text;

namespace Services
{
    public class OldCalculator
    {
        const double LeadDensity = 11340;    // Density of lead in kg/m3
        const double _pi_ = 3.1415926;
        
        //const double drag_coeff = 9.7521;   // Arbitrary drag coeff. This makes the British Retardation data match real-world data. DON'T CHANGE!
        
        const double BC = 1.0;  // Relative ballistic coefficient. Tweak this if you must, not 'drag_coeff'
        const int data_interval = 1;    // Data every x meters
        const double std_temp_c = 21.0; // 'Standard' sea-level temperature
        const double temp_lapse_rateCperM = 6.47E-3; // Environmental lapse rate in deg C/m
        const double temp_lapse_rate = 3.55E-3; // Environmental lapse rate in deg F/ft

        public IList<FlightPosition> CalculateFromFPS(double muzzleVelocityFPS, double caliberIn, double weightGrains, double windSpeedMPH, double temperatureInF, double altitudeInFeet)
        {
            const int max_intervals = 1000;
            const double time_interval = 0.001; // Integration interval
            const double g_English = 32.174;	// g - in English units ft/sec/sec
            const double grains_per_pound = 7000;// Grains per pound
            const double mph_to_fps = 1.46667;	// MPH to FPS conversion
            const double drag_coeff = 0.0755;   // Arbitrary drag coeff. This makes the British Retardation data match real-world data. DON'T CHANGE!

            IList<FlightPosition> result = new List<FlightPosition>();

            double hVel = muzzleVelocityFPS;  // Horizontal velocity
            double hPosn = 0;   // Horizontal distance
            double vPosn = 0;   // Vertical distance
            double xVel = 0;        // Cross-velocity
            double xPosn = 0;   // Cross-position
            double currTime = 0;
            double bulletMass = weightGrains / (grains_per_pound * g_English);
            double bulletArea = caliberIn * caliberIn * _pi_ / 4.0;
            //mSlope = 0.0;

            //mVelocity.clear();
            //mDrop.clear();
            //mDrift.clear();
            //mTof.clear();

            int intervalNum = 0;
            int distanceMeters = 0;

            while (intervalNum < max_intervals)
            {
                // To
                currTime = intervalNum * time_interval;

                // Increment distance traveled over interval
                hPosn += hVel * time_interval;
                xPosn += xVel * time_interval;

                // Vertical fall due to gravity
                // s = 0.5 at^2
                vPosn = 0.5 * g_English * currTime * currTime;

                // Find force on bullet
                // Total velocity is the vector sum of the velocity and crosswind
                double xFPS = windSpeedMPH * mph_to_fps - xVel;
                double vT = Math.Sqrt(hVel * hVel + xFPS * xFPS);
                double force = VelocityFPSRangeCoefitient(vT) * bulletArea * drag_coeff * calcRelDensity(altitudeInFeet, temperatureInF) / BC;

                // Resolve force into cross and forward vectors
                double xForce = force * xFPS / hVel;
                double hForce = force * (hVel - xFPS) / hVel;

                double hAccel = hForce / bulletMass;
                double xAccel = xForce / bulletMass;
                double deltaHV = hAccel * time_interval;
                double deltaXV = xAccel * time_interval;
                hVel -= deltaHV;
                xVel += deltaXV;

                // See if we are at a yard marker - we take data every yard...
                if ((hPosn * 0.3048) > (double)(distanceMeters * data_interval))
                {
                    //mVelocity.append(vT);
                    //mDrift.append(xPosn * in_per_foot);
                    //mDrop.append(vPosn * in_per_foot);
                    //mTof.append(currTime);  // TOF in seconds
                    result.Add(new FlightPosition
                    {
                        Velocity = vT * 0.3048,
                        Time = currTime,
                        Distance = distanceMeters,
                        Drift = xPosn * 0.3048,
                        Drop = vPosn * 0.3048
                    });
                    ++distanceMeters;
                }

                ++intervalNum;
            }
            /* 
                Calculate the angle of the barrel based upon the drop of the
                projectule at the aim point. The total drop is the ballistic
                drop plus the sight height
            */
            // The slope is measured in inches/yard
            //int posn = (int)mAimPoint / data_interval;
            //if (mSlope == 0.0)
            //{
            //    mSlope = (mDrop.at(posn) + mSightHeight) / mAimPoint;
            //}
            return result;
        }

        private double calcRelDensity(double altitudeInFeet, double temperatureInF)
        {
            const double std_pres = 14.7;   // Standard air pressure at sea level

            // Calculate the air pressure at the altitude
            //double alt = ui.mAltitude->itemData(ui.mAltitude->currentIndex()).toDouble();
            double pres = 14.7 * Math.Pow((1 - 6.8877E-6 * altitudeInFeet), 5.25588);   // Pressure at altitude in ft.

            // Adjust pressure for non-standard temp
            const double abs_f = 459.7; // 
            double relTP = (abs_f + lapsedTempF(altitudeInFeet)) / (abs_f + temperatureInF);
            pres *= relTP;  // Pressure adjusted for temperature
            return pres / std_pres;
        }

        private double lapsedTempF(double alt)
        {
            const double std_temp_f = 70.0;	// 'Standard' sea-level temperature
            return std_temp_f - temp_lapse_rate * alt;
        }

        private double VelocityFPSRangeCoefitient(double velocity)
        {
            // British data
            const double fact_m1 = 74422 / 1.0E8;
            const double fact_m2 = 59939 / 1.0E12;
            const double fact_m3 = 23385 / 1.0E22;
            const double fact_m4 = 95408 / 1.0E12;
            const double fact_m5 = 59814 / 1.0E8;
            const double fact_m6 = 58497 / 1.0E7;
            const double fact_m7 = 15366 / 1.0E7;
            const double expn_1 = 1.6;
            const double expn_2 = 3.0;
            const double expn_3 = 6.45;
            const double expn_4 = 3.0;
            const double expn_5 = 1.8;
            const double expn_6 = 1.5;
            const double expn_7 = 1.67;

            if (velocity <= 840)
            {
                return Math.Pow(velocity, expn_1) * fact_m1;
            }
            else if (velocity <= 1040)
            {
                return Math.Pow(velocity, expn_2) * fact_m2;
            }
            else if (velocity <= 1190)
            {
                return Math.Pow(velocity, expn_3) * fact_m3;
            }
            else if (velocity <= 1460)
            {
                return Math.Pow(velocity, expn_4) * fact_m4;
            }
            else if (velocity <= 2000)
            {
                return Math.Pow(velocity, expn_5) * fact_m5;
            }
            else if (velocity <= 2600)
            {
                return Math.Pow(velocity, expn_6) * fact_m6;
            }
            else
            {
                return Math.Pow(velocity, expn_7) * fact_m7;
            }
        }

        public IList<FlightPosition> Calculate(double muzzleVelocity, double caliber, double materialDensity, double windSpeed, double temperature, double altitude)
        {
            const int max_intervals = 2000;
            const double time_interval = 0.001;
            const double GAcceleration = 9.81;
            const double drag_coeff = 70.537;   // 70.537Arbitrary drag coeff. This makes the British Retardation data match real-world data. DON'T CHANGE!

            double horisontalVelosity = muzzleVelocity;  // Horizontal velocity
            double horisontalPosition = 0;   // Horizontal distance
            double verticalPosition = 0;   // Vertical distance
            double crossVelocity = 0;        // Cross-velocity
            double crossPosition = 0;   // Cross-position
            double currTime = 0;
            double sphereVolume = 4 * Math.Pow(caliber / 2, 3) * _pi_ / 3;
            double sphereMass = materialDensity * sphereVolume;
            double sphereArea = ((caliber / 2) * (caliber / 2)) * _pi_;
            //mSlope = 0.0;
            IList<FlightPosition> result = new List<FlightPosition>();
            //mVelocity.clear();
            //mDrop.clear();
            //mDrift.clear();
            //mTof.clear();

            int intervalNum = 0;
            int distance = 0;

            while (intervalNum < max_intervals)
            {
                // To
                currTime = intervalNum * time_interval;

                // Increment distance traveled over interval
                horisontalPosition += horisontalVelosity * time_interval;
                crossPosition += crossVelocity * time_interval;

                // Vertical fall due to gravity
                // s = 0.5 at^2
                verticalPosition = GAcceleration * currTime * currTime / 2;

                // Find force on bullet
                double windSpeedOnBullet = windSpeed - crossVelocity;
                // Total velocity is the vector sum of the velocity and crosswind
                double totalVelocity = Math.Sqrt(horisontalVelosity * horisontalVelosity + windSpeedOnBullet * windSpeedOnBullet);
                double force = totalVelocity * sphereArea * drag_coeff * calcRelativeDensity(temperature, altitude) / BC;
                //(VelocityFPSRangeCoefitient(totalVelocity/0.3048) * 0.3048)
                //double hVel0 = horisontalVelosity - ((force * ((horisontalVelosity - windSpeedOnBullet) / horisontalVelosity)) / sphereMass) * time_interval;
                // Resolve force into cross and forward vectors
                double xForce = force * windSpeedOnBullet / horisontalVelosity;
                //double hForce = force * (horisontalVelosity - windSpeedOnBullet) / horisontalVelosity;

                //double hAccel = hForce / sphereMass;
                double xAccel = xForce / sphereMass;
                //double deltaHV = hAccel * time_interval;
                double deltaXV = xAccel * time_interval;
                //horisontalVelosity -= deltaHV;
                horisontalVelosity -= ((force * ((horisontalVelosity - windSpeedOnBullet) / horisontalVelosity)) / sphereMass) * time_interval;
                crossVelocity += deltaXV;

                // See if we are at a yard marker - we take data every yard...
                if (horisontalPosition > (distance * data_interval))
                {
                    result.Add(new FlightPosition
                    {
                        Velocity = totalVelocity,
                        Time = currTime,
                        Distance = distance,
                        Drift = crossPosition * 0.3048,
                        Drop = verticalPosition * 0.3048
                    });
                    //mVelocity.append(vT);
                    //mDrift.append(xPosn * in_per_foot);
                    //mDrop.append(vPosn * in_per_foot);
                    //mTof.append(currTime);  // TOF in seconds
                    ++distance;
                }

                ++intervalNum;
            }
            /* 
                Calculate the angle of the barrel based upon the drop of the
                projectule at the aim point. The total drop is the ballistic
                drop plus the sight height
                */
            // The slope is measured in inches/yard
            //int posn = (int)mAimPoint / data_interval;
            //if (mSlope == 0.0)
            //{
            //    mSlope = (mDrop.at(posn) + mSightHeight) / mAimPoint;
            //}
            return result;
        }

        private double VelocityRangeCoefitient(double velocity)
        {
            double result;
            if (velocity <= 256)
            {
                result = Math.Pow(velocity, 1.6);
                return result * 1.1120524653527189311976639706795E-4;
            }
            else if (velocity <= 317)
            {
                result = Math.Pow(velocity, 3);
                return result‬ * 0.000000001697283467877888;
            }
            else if (velocity <= 363)
            {
                result = Math.Pow(velocity, 6.45);
                return result * 3.4943090621420187855818671836744e-19;
            }
            else if (velocity <= 445)
            {
                result = Math.Pow(velocity, 3);
                return result‬‬ * 1.4256362582888421137258703624546e-8;
            }
            else if (velocity <= 610)
            {
                result = Math.Pow(velocity, 1.8);
                return result * 8.9377208570862823023644987694803e-5;
            }
            else if (velocity <= 792)
            {
                result = Math.Pow(velocity, 1.5);
                return result * 8.740927825876488043625507147462e-4;
            }
            else
            {
                result = Math.Pow(velocity, 1.67);
                return result * 2.2960681226801052238294193348018e-4;
            }
        }

        private double lapsedTemp(double alt)
        {
            return std_temp_c - temp_lapse_rateCperM * alt;
        }

        private double calcRelativeDensity(double temperature, double altitude)
        {
            const double std_pres = 101325;   // Standard air pressure at sea level

            // Calculate the air pressure at the altitude
            double pres = std_pres * Math.Pow((1 - 22.59744E-6 * altitude), 5.25588);   // Pressure at altitude in ft.

            // Adjust pressure for non-standard temp
            const double abs_C = 273.15;
            double relTP = (abs_C + lapsedTemp(altitude)) / (abs_C + temperature);
            pres *= relTP;  // Pressure adjusted for temperature
            //return pres / std_pres;
            return 1;
        }
    }
}
