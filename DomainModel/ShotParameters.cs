﻿using System;

namespace DomainModel
{
    public class ShotParameters
    {
        public int MuzzleVelocity { get; set; }

        public decimal SphereDiameter { get; set; }

        public decimal AirTemperature { get; set; }

        public decimal AirPressure { get; set; }

        public decimal RelativeAirHumidity { get; set; }

        public decimal Crosswind { get; set; }

        public decimal MaterialDensity { get; set; }
    }
}
